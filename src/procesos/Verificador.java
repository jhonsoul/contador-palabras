/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package procesos;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import javax.swing.DefaultListModel;
import javax.swing.ListModel;

/**
 *
 * @author Jhon
 */
public class Verificador {
    
    private String texto;
    private String textoLimpio;

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }
    
    private void limpiarTexto(){
        textoLimpio = texto;
        textoLimpio = textoLimpio.replaceAll("[^a-zA-Z0-9À-ÿ ]", "").toLowerCase();
    }
    
    private Map contadorPalabras() {
        limpiarTexto();
        Map<String, Integer> diccionario = new HashMap<>();
        for ( String palabra : textoLimpio.split(" ") ) {
            Integer oldCount = diccionario.get(palabra);
            if ( oldCount == null ) {
               oldCount = 0;
            }
            diccionario.put(palabra, oldCount + 1);
        }
        return diccionario;
    }
    
    public ListModel cargarLista() {
        Map diccionario = contadorPalabras();
        Iterator lista = diccionario.keySet().iterator();
        DefaultListModel modelo = new DefaultListModel();
        while (lista.hasNext()) {
            String key = lista.next().toString();
            modelo.addElement(key + " -> cantidad: " + diccionario.get(key));
        }
        return modelo;
    }
}
